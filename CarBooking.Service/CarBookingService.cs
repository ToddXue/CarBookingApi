﻿using System;
using System.Collections.Generic;
using System.Linq;
using CarBooking.Common.Models;
using CarBooking.Service.Interfaces;
using CarBooking.Service.ResponseTypes;
using CarBooking.DataAccess;

namespace CarBooking.Service
{
    public class CarBookingService : ICarBookingService
    {
        private readonly IRepository<Car> _repository;

        public CarBookingService(IRepository<Car> repository)
        {
            _repository = repository;
        }

        public ServiceResult BookCar(int carId, string bookedBy)
        {
            var result = new ServiceResult();
            try
            {
                var carToBook = _repository.GetById(carId);
                if (carToBook.LastBookedAt.AddHours(24) < DateTime.Now)
                {
                    carToBook.LastBookedAt = DateTime.Now;
                    carToBook.BookedBy = bookedBy;


                    _repository.Update(carToBook);
                    result.IsSuccess = true;
                }
                else
                {
                    result.IsSuccess = false;
                    result.ErrorMessage = "The selected car has just been booked by someone else.";
                }
            }
            catch (Exception ex)
            {
                result.IsSuccess = false;
                result.ErrorMessage = ex.Message;
            }

            return result;
        }

        /// <summary>
        /// Return the car. Not required by the task but it is handy for testing functionalities.  
        /// </summary>
        /// <param name="carId"></param>
        /// <returns></returns>
        public ServiceResult ReturnCar(int carId)
        {
            var result = new ServiceResult();
            try
            {
                var carToBook = _repository.GetById(carId);
                carToBook.LastBookedAt = DateTime.Now.AddHours(-24);
                carToBook.BookedBy = string.Empty;


                _repository.Update(carToBook);
                result.IsSuccess = true;
            }
            catch (Exception ex)
            {
                result.IsSuccess = false;
                result.ErrorMessage = ex.Message;
            }

            return result;
        }

        public IEnumerable<Car> GetAllAvailableCars()
        {
            return _repository.GetAll().Where(car => car.LastBookedAt.AddHours(24) < DateTime.Now).OrderBy(car => car.Id);
        }
    }
}
