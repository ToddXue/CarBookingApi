﻿using System.Collections.Generic;
using CarBooking.Common.Models;
using CarBooking.Service.ResponseTypes;

namespace CarBooking.Service.Interfaces
{
    public interface ICarBookingService
    {
        IEnumerable<Car> GetAllAvailableCars();
        ServiceResult BookCar(int carId, string bookedBy);
        ServiceResult ReturnCar(int carId);
    }
}
