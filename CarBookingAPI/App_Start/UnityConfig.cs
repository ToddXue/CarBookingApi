﻿using System;
using System.Web.Http;
using CarBooking.Ioc;
using CarBookingAPI.DependencyResolver;
using Microsoft.Practices.Unity;

namespace CarBookingAPI
{
    public class UnityConfig
    {
        private static readonly Lazy<IUnityContainer> Container = new Lazy<IUnityContainer>(() =>
        {
            var container = new UnityContainer();

            RegisterTypes(container);
            return container;
        });

        /// <summary>
        /// Gets the configured Unity containerRegister.
        /// </summary>
        public static IUnityContainer GetConfiguredContainer()
        {
            return Container.Value;
        }

        /// <summary>Registers the type mappings with the Unity containerRegister.</summary>
        /// <param name="containerRegister">The unity containerRegister to configure.</param>
        /// <remarks>There is no need to register concrete types such as controllers or API controllers (unless you want to 
        /// change the defaults), as Unity allows resolving a concrete type even if it was not previously registered.</remarks>
        private static void RegisterTypes(IUnityContainer containerRegister)
        {
            var unityRegister = new CarBookingServiceRegister();
            unityRegister.UnityRegister(containerRegister);
        }

        public static void Register()
        {
            var container = GetConfiguredContainer();

            System.Web.Mvc.DependencyResolver.SetResolver(new UnityDependencyResolver(container));

            Microsoft.Practices.ServiceLocation.ServiceLocator.SetLocatorProvider(() => new UnityServiceLocator(container));

            GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container);

        }
    }
}