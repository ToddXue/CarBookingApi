﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CarBooking.Common.Models;

namespace CarBookingAPI.Models
{
    public class CarListViewModel
    {
        public int Count => Cars.Count();
        public IEnumerable<Car> Cars { get; set; }
    }
}