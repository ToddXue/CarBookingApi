﻿using System.Collections.Generic;
using System.Web.Http;
using CarBooking.Common.Models;
using CarBooking.Service.Interfaces;
using CarBooking.Service.ResponseTypes;
using CarBookingAPI.Models;

namespace CarBookingAPI.Controllers
{
    public class CarBookingController : ApiController
    {
        private readonly ICarBookingService _bookingService;

        public CarBookingController(ICarBookingService bookingService)
        {
            this._bookingService = bookingService;
        }

        [HttpGet, Route("list")]
        public CarListViewModel GetAllAvailableCars()
        {
            var availableCars = _bookingService.GetAllAvailableCars();
            return new CarListViewModel() {Cars = availableCars};
        }

        [HttpGet, Route("book/{id}")]
        public ServiceResult BookCar(int id, string bookedBy = "Default User")
        {
            return _bookingService.BookCar(id, bookedBy);
        }

        [HttpGet, Route("return/{id}")]
        public ServiceResult ReturnCar(int id)
        {
            return _bookingService.ReturnCar(id);
        }
    }
}
