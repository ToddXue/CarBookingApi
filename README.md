This API is for Demo purpose only
Simply run it in Visual Studio and test the URLs in brower. 
I make all method httpGet, although in reallity BookCar would probably be a POST method, but get is easier for debug and demo.

Given that I had some network and proxy issues getting the Nuget Packages, I also include the package folder in the solution for your conveniency.

****************************************************************************************************************
Assumptions:
The number of total cars should be a reasonable number (e.g. less than 1000) so query performance wouldn't be a problem.

Although it is not required by the task to maintain who booked the car, neither do we support return a car before 24 hours elapsed, 
I assume they are necessary in fact and that also helps with debugging and testing, so I spend a little bit effort to include that

No DB is used in this project, I created a list of 5 cars for demo purpose. 
In reality there will be a relational database sitting behind it.  As as result, the code often acts like they were interact with a database. 

Cross Site Origin is taken into consideration as this is an API and it will be consumed by web apps and native phone apps.

I only use HTTP Get for all API calls.

****************************************************************************************************************

If I had more time, the following improvements should be considered
1. Use .NET CORE to build this app. 
2. Use Entity Framework to populate a database, and the data access layer implementation class will need to be improved, e.g. use of UoW.
3. When we use a database and there are more data, I can convert all the methods to be async. But for now we don't need to.
3. I've also considered concurrent access. With the current solution that should be fine. When a user tries to book a car that has just been 
 booked by otheres, he will get a proper error message. But when there are more features added, we needs to be careful about this. I considered
 using c# lock, or some other ways, but there will be performance punishment and the best place to take care of the concurrency control should be in Database configuration.
4. When the solution grows bigger, we will need unit test coverage for the CarBooking.Common project too.  whether we should have unit test 
 for Data Access project is in doubt. But with the current solution, test coverage over controllers and services should be enough.
5. Should use http POST for car booking, because in reality, to book a car user probably needs to fill in some forms.
 
****************************************************************************************************************

For testing purpose, the below route can be used

* to return a list of available cars
* /list

* to book an available car
* /book/{id}
* where id = 1 - 5
* trying to book an unavailable car or invalid id will give you a proper error message

* you may also manually return a car and make it available by
* /return/{id}
