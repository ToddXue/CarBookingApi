﻿using System.Collections.Generic;
using CarBooking.Common.Models;

namespace CarBooking.DataAccess
{
    public interface IRepository<T> where T : EntityBase
    {
        IEnumerable<T> GetAll();

        T GetById(int id);

        void Create(T entity);

        void Delete(T entity);

        void Update(T entity);
    }
}
