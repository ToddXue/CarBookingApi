﻿using System;

namespace CarBooking.Common.Models
{
    public class Car : EntityBase
    {
        public Car(int id, string description)
        {
            this.Id = id;
            this.Description = description;
        }

        public string Description { get; private set; }
        public DateTime LastBookedAt { get; set; }
        public string BookedBy { get; set; } // this is not required by the task but in reality this field will be needed so I leave it here.
    }
}
