﻿using System;

namespace CarBooking.Common.Exceptions
{
    public class IdNotFoundException : ApplicationException
    {
        public IdNotFoundException(string message) : base(message) { }
    }
}
