﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using CarBooking.Common.Models;
using CarBooking.Service.Interfaces;
using CarBooking.Service.ResponseTypes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CarBookingAPI.API;
using CarBookingAPI.Controllers;
using Moq;

namespace CarBookingAPI.API.Tests.Controllers
{
    [TestClass]
    public class CarBookingControllerTest
    {
        private Mock<ICarBookingService> _mockBookingService;
        private IEnumerable<Car> _availableCars;

        [TestInitialize]
        public void testInit()
        {
            _availableCars = new List<Car>()
            {
                new Car(1, "first car"),
                new Car(2, "second car"),
            };
            _mockBookingService = new Mock<ICarBookingService>();
        }

        [TestMethod]
        public void ListShouldReturnAllAvailableCars()
        {
            // Arrange
            _mockBookingService.Setup(x => x.GetAllAvailableCars()).Returns(_availableCars);
            CarBookingController controller = new CarBookingController(_mockBookingService.Object);

            // Act
            var result = controller.GetAllAvailableCars();

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(2, result.Count);
        }

        [TestMethod]
        public void BookCarReturnServiceResult()
        {
            // Arrange
            _mockBookingService.Setup(x => x.BookCar(It.IsAny<int>(), It.IsAny<string>())).Returns(new ServiceResult());
            CarBookingController controller = new CarBookingController(_mockBookingService.Object);

            // Act
            var result = controller.BookCar(1);

            // Assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(ServiceResult));
        }

        [TestMethod]
        public void ReturnCarReturnServiceResult()
        {
            // Arrange
            _mockBookingService.Setup(x => x.ReturnCar(It.IsAny<int>())).Returns(new ServiceResult());
            CarBookingController controller = new CarBookingController(_mockBookingService.Object);

            // Act
            var result = controller.ReturnCar(1);

            // Assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(ServiceResult));
        }
    }
}
