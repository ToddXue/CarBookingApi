﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarBooking.Ioc
{
    public interface IDependencyRegistor<T>
    {
        void UnityRegister(T container);
    }
}
