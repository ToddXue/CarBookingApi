﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarBooking.Common.Models;
using CarBooking.DataAccess;
using CarBooking.Service;
using CarBooking.Service.Interfaces;
using Microsoft.Practices.Unity;

namespace CarBooking.Ioc
{
    public class CarBookingServiceRegister : IDependencyRegistor<IUnityContainer>
    {
        public void UnityRegister(IUnityContainer container)
        {
            container.RegisterType<IRepository<Car>, CarRepository>(
                new ContainerControlledLifetimeManager(),
                new InjectionFactory(c => CarRepository.Instance)
                );
            container.RegisterType<ICarBookingService, CarBookingService>(
                new InjectionConstructor(
                    new ResolvedParameter<IRepository<Car>>()
                    )
                );
        }
    }
}
