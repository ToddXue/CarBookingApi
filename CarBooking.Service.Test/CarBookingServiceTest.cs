﻿using System;
using System.Collections.Generic;
using System.Linq;
using CarBooking.Common.Exceptions;
using CarBooking.Common.Models;
using CarBooking.DataAccess;
using CarBooking.Service.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace CarBooking.Service.Test
{
    [TestClass]
    public class CarBookingServiceTest
    {
        private Mock<IRepository<Car>> _mockCarRepository;
        private IEnumerable<Car> _availableCars;

        [TestInitialize]
        public void testInit()
        {
            _availableCars = new List<Car>()
            {
                new Car(1, "first car")
                {
                    LastBookedAt = DateTime.MinValue
                },
                new Car(2, "second car")
                {
                    LastBookedAt = DateTime.Now.AddHours(-23)
                },
                new Car(3, "third car")
                {
                    LastBookedAt = DateTime.Now.AddHours(-25)
                }
            };
            _mockCarRepository = new Mock<IRepository<Car>>();
        }

        [TestMethod]
        public void GetAllAvailableCarsReturnsAllCarsAvailable()
        {
            // Arrange
            _mockCarRepository.Setup(x => x.GetAll()).Returns(_availableCars);
            var carBookingService = new CarBookingService(_mockCarRepository.Object);

            // Act
            var result = carBookingService.GetAllAvailableCars();

            // Assert
            Assert.AreEqual(2, result.Count());

        }

        [TestMethod]
        public void BookCarReturnsSuccessfulResultWhenBookAvailableCar()
        {
            // Arrange
            _mockCarRepository.Setup(x => x.GetById(1))
                .Returns(new Car(1, "first car")
                            {
                                LastBookedAt = DateTime.MinValue
                            });
            var carBookingService = new CarBookingService(_mockCarRepository.Object);

            // Act
            var result = carBookingService.BookCar(1, "");

            // Assert
            _mockCarRepository.Verify(m => m.Update(It.IsAny<Car>()));
            Assert.IsTrue(result.IsSuccess);
        }

        [TestMethod]
        public void BookCarReturnsNotSuccessfulResultWhenCarIsNotAvailable()
        {
            // Arrange
            _mockCarRepository.Setup(x => x.GetById(2))
                .Returns(new Car(2, "second car")
                {
                    LastBookedAt = DateTime.Now.AddHours(-23)
                });
            var carBookingService = new CarBookingService(_mockCarRepository.Object);

            // Act
            var result = carBookingService.BookCar(2, "");

            // Assert
            _mockCarRepository.Verify(m => m.Update(It.IsAny<Car>()), Times.Never);
            Assert.IsFalse(result.IsSuccess);
            Assert.AreEqual("The selected car has just been booked by someone else.", result.ErrorMessage);
        }

        [TestMethod]
        public void BookCarReturnsNotSuccessfulResultWhenExcetpionCaught()
        {
            // Arrange
            _mockCarRepository.Setup(x => x.GetById(1))
                .Returns(new Car(1, "first car")
                {
                    LastBookedAt = DateTime.MinValue
                });
            _mockCarRepository.Setup(x => x.Update(It.IsAny<Car>())).Throws(new IdNotFoundException("for test."));
            var carBookingService = new CarBookingService(_mockCarRepository.Object);

            // Act
            var result = carBookingService.BookCar(1, "");

            // Assert
            Assert.IsFalse(result.IsSuccess);
            Assert.AreEqual("for test.", result.ErrorMessage);
        }

        [TestMethod]
        public void ReturnCarReturnsSuccessfulResult()
        {
            // Arrange
            _mockCarRepository.Setup(x => x.GetById(It.IsAny<int>()))
                .Returns(new Car(1, "first car")
                {
                    LastBookedAt = DateTime.MinValue
                });
            var carBookingService = new CarBookingService(_mockCarRepository.Object);

            // Act
            var result = carBookingService.ReturnCar(2);

            // Assert
            _mockCarRepository.Verify(m => m.Update(It.IsAny<Car>()));
            Assert.IsTrue(result.IsSuccess);
        }

        [TestMethod]
        public void ReturnCarReturnsUnsuccessfulResultIfExcetptionThrown()
        {
            // Arrange
            _mockCarRepository.Setup(x => x.GetById(It.IsAny<int>()))
                .Returns(new Car(1, "first car")
                {
                    LastBookedAt = DateTime.MinValue
                });
            _mockCarRepository.Setup(x => x.Update(It.IsAny<Car>())).Throws(new IdNotFoundException("for test."));
            var carBookingService = new CarBookingService(_mockCarRepository.Object);

            // Act
            var result = carBookingService.ReturnCar(2);

            // Assert
            _mockCarRepository.Verify(m => m.Update(It.IsAny<Car>()));
            Assert.IsFalse(result.IsSuccess);
            Assert.AreEqual("for test.", result.ErrorMessage);
        }
    }
}
